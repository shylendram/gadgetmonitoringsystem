package com.gk.gms.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gk.gms.java.EntityManagerProvider;
import com.gk.gms.pojoclasses.CustomerInfo;

public class SendCustomerDetailsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String customerId = request.getParameter("customerId");
		String serviceProviderId = request.getParameter("serviceProviderId");
		String productId = request.getParameter("productId");
		String manufacturerId = request.getParameter("manufacturerId");

		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();

		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setServiceProviderId(serviceProviderId);
		customerInfo.setManufacturerId(manufacturerId);
		customerInfo.setCustomerId(customerId);
		customerInfo.setProductId(productId);
		entityManager.persist(customerInfo);

		entityManager.getTransaction().commit();
		response.sendRedirect("jsp/success.jsp");
	}

}
