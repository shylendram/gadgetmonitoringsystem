package com.gk.gms.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gk.gms.java.EntityManagerProvider;
import com.gk.gms.pojoclasses.Complaint;

public class ProvideSolution extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String cid = request.getParameter("cId");
		String solution = request.getParameter("solution");

		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();

		Complaint complaint = entityManager.find(Complaint.class, Long.parseLong(cid));
		complaint.setSoluntionDescription(solution);
		entityManager.persist(complaint);

		entityManager.getTransaction().commit();
		response.sendRedirect("jsp/success.jsp");
	}



}
