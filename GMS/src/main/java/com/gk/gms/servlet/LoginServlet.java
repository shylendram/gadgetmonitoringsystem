package com.gk.gms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gk.gms.java.Validate;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("uname");
		String password = request.getParameter("password");
		String usertype = request.getParameter("usertype");

		if (usertype.equals("customer")) {
			int custId = Validate.verifyCusomerLogin(username, password);
			
			if (custId !=  -1) {
				HttpSession session = request.getSession(true);
				session.setAttribute("cId", custId);
				response.sendRedirect("jsp/CustomerSuccessLoginPage.jsp");
			} else {
				response.sendRedirect("jsp/LoginFailed.jsp");
			}
		}
		if (usertype.equals("service_provider")) {
			int sid = Validate.verifyServiceProviderLogin(username, password);
			if (sid != -1) {
				HttpSession session = request.getSession(true);
				session.setAttribute("sId", sid);
				response.sendRedirect("jsp/ServiceProviderSuccessLoginPage.jsp");
			} else {
				response.sendRedirect("jsp/LoginFailed.jsp");
			}
		}
		if (usertype.equals("manufacturer")) {
			int mid = Validate.verifyManufacturerLogin(username, password);
			if (mid != -1) {
				HttpSession session = request.getSession(true);
				session.setAttribute("mId", mid);
				response.sendRedirect("jsp/ManufacturerSuccessLoginPage.jsp");
			} else {
				response.sendRedirect("jsp/LoginFailed.jsp");
			}
		}
	}

}
