package com.gk.gms.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gk.gms.java.EntityManagerProvider;
import com.gk.gms.pojoclasses.Complaint;

public class ComplaintServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String customerId = String.valueOf(request.getSession().getAttribute(
				"cId"));
		String complaintType = request.getParameter("complaintType");
		String manufacturerId = request.getParameter("manufacturerId");
		String complaintDescription = request.getParameter("complaintDescription");

		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();

		Complaint complaint = new Complaint();
		complaint.setCustomerId(customerId);
		complaint.setManufacturerId(manufacturerId);
		complaint.setComplaintType(complaintType);
		complaint.setComplaintDescription(complaintDescription);
		entityManager.persist(complaint);

		entityManager.getTransaction().commit();
		response.sendRedirect("jsp/success.jsp");
	}

}
