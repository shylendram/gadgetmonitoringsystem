package com.gk.gms.servlet;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gk.gms.java.EntityManagerProvider;
import com.gk.gms.pojoclasses.Customer;
import com.gk.gms.pojoclasses.Manufacturer;
import com.gk.gms.pojoclasses.ServiceProvider;

public class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastname");
		String userName = req.getParameter("username");
		String password = req.getParameter("password");
		String mailId = req.getParameter("mail");
		String userType = req.getParameter("usertype");

		EntityManager entityManager = EntityManagerProvider.getEntityManager();
		entityManager.getTransaction().begin();

		if (userType.equals("customer")) {
			Customer customer = new Customer();
			customer.setFirstname(firstName);
			customer.setLastname(lastName);
			customer.setUsername(userName);
			customer.setPassword(password);
			customer.setMail(mailId);
			entityManager.persist(customer);

		}
		if (userType.equals("service_provider")) {
			ServiceProvider serviceprovider = new ServiceProvider();
			serviceprovider.setFirstname(firstName);
			serviceprovider.setLastname(lastName);
			serviceprovider.setUsername(userName);
			serviceprovider.setPassword(password);
			serviceprovider.setMail(mailId);
			entityManager.persist(serviceprovider);
		}
		if (userType.equals("manufacturer")) {
			Manufacturer manufacturer = new Manufacturer();
			manufacturer.setFirstname(firstName);
			manufacturer.setLastname(lastName);
			manufacturer.setUsername(userName);
			manufacturer.setPassword(password);
			manufacturer.setMail(mailId);
			entityManager.persist(manufacturer);
		}

		resp.sendRedirect("Success.html");
		entityManager.getTransaction().commit();
	}
}
