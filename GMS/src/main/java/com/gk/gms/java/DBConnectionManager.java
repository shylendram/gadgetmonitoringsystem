package com.gk.gms.java;

import java.sql.Connection;
import java.sql.DriverManager;

import com.mysql.jdbc.log.Log;

public final class DBConnectionManager {
	private DBConnectionManager() {}
	
	static String url = "jdbc:mysql://localhost:3306/";
	static String dbName = "gms";
	static String driver = "com.mysql.jdbc.Driver";
	static String userName = "root";
	static String pswd = "root";
	private static Connection con = null;
	Log logger;
	
	public static Connection getConnection() {
		try {
			Class.forName(driver).getInterfaces();
			con = DriverManager.getConnection(url+dbName , userName , pswd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}