package com.gk.gms.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.gk.gms.pojoclasses.Complaint;

public class SeeComplaintDetails {

	public static List<Complaint> GetComplaintDetails(String manufacturerId) {
		Statement st = null;
		Connection conn = null;
		List<Complaint> clist = new ArrayList<Complaint>();
		try {

			conn = DBConnectionManager.getConnection();
			st=conn.createStatement();
			ResultSet mresult = null;
			mresult = st
					.executeQuery("select * from complaint where manufacturerId='"
							+ manufacturerId + "'");
			while (mresult.next()) {
				Complaint ci = new Complaint();
				ci.setComplaintId(Long.parseLong(mresult.getString(1)));
				ci.setCustomerId(mresult.getString(2));
				ci.setComplaintType(mresult.getString(4));
				ci.setComplaintDescription(mresult.getString(5));
				clist.add(ci);
			}

		} catch (SQLException sqle) {
			System.out.println(sqle);
		}
		return clist;
	}

	public static Complaint GetComplaint(String cid) {
		Statement st = null;
		Connection conn = null;
		try {

			conn = DBConnectionManager.getConnection();
			st=conn.createStatement();
			ResultSet mresult = null;
			mresult = st
					.executeQuery("select * from complaint where complaintId='"
							+ cid + "'");
			if (mresult.next()) {
				Complaint ci = new Complaint();
				ci.setCustomerId(mresult.getString(2));
				ci.setComplaintType(mresult.getString(4));
				ci.setComplaintDescription(mresult.getString(5));
				ci.setSoluntionDescription(mresult.getString(6));
				return ci;
			}

		} catch (SQLException sqle) {
			System.out.println(sqle);
		}
		return null;
	}

}
