package com.gk.gms.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.gk.gms.pojoclasses.ProductInfo;

public class ValidateCustomerProduct {
	public static List<ProductInfo> verifyCustomerProductRequest(
			String customerId) {
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		List<ProductInfo> plist = new ArrayList<ProductInfo>();
		try {

			conn = DBConnectionManager.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery("select productId from customer_product where customerId='"
					+ customerId + "'");
			while (rs.next()) {
				long productid = rs.getLong(1);
				ProductInfo pi = new ProductInfo();
				st = conn.createStatement();
				ResultSet presult = null;
				presult = st
						.executeQuery("select * from product_info where pid='"
								+ productid + "'");
				if (presult.next()) {
					pi.setId(presult.getLong(1));
					pi.setName(presult.getString(2));
					pi.setType(presult.getString(3));
					pi.setCost(presult.getLong(4));
					plist.add(pi);
				}
			}

		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return plist;
	}
}