package com.gk.gms.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.gk.gms.pojoclasses.Customer;

public class ManufacturerCustomerDetails {
	
	public static List<Customer> getManufacturerCustomerDetails(
			String manufacturerId) {
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		List<Customer> clist = new ArrayList<Customer>();
		try {

			conn = DBConnectionManager.getConnection();
			st = conn.createStatement();
			rs = st.executeQuery("select customerId from customer_info where manufacturerId='"
					+ manufacturerId + "'");
			while (rs.next()) {
				long manufacturerId1 = rs.getLong(1);
				Customer ci = new Customer();
				st = conn.createStatement();
				ResultSet presult = null;
				presult = st
						.executeQuery("select * from customer where cid='"
								+ manufacturerId1 + "'");
				if (presult.next()) {
					ci.setCid(presult.getLong(1));
					ci.setFirstname(presult.getString(2));
					ci.setLastname(presult.getString(3));
					ci.setMail(presult.getString(6));
					clist.add(ci);
				}
			}

		} catch (SQLException sqle) {
			System.out.println(sqle);
		}
		return clist;
	}


}
