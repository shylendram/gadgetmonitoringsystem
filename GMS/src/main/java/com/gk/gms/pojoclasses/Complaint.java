package com.gk.gms.pojoclasses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "complaint")
public class Complaint {
	@Id
	@GeneratedValue
	private Long complaintId;
	@Column
	private String customerId;
	@Column
	private String manufacturerId;
	@Column
	private String complaintType;
	@Column
	private String complaintDescription;
	@Column
	private String soluntionDescription;
	public Long getComplaintId() {
		return complaintId;
	}
	public void setComplaintId(Long complaintId) {
		this.complaintId = complaintId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId2) {
		this.customerId = customerId2;
	}
	public String getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(String manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public String getComplaintType() {
		return complaintType;
	}
	public void setComplaintType(String complaintType) {
		this.complaintType = complaintType;
	}
	public String getComplaintDescription() {
		return complaintDescription;
	}
	public void setComplaintDescription(String complaintDescription) {
		this.complaintDescription = complaintDescription;
	}
	public String getSoluntionDescription() {
		return soluntionDescription;
	}
	public void setSoluntionDescription(String soluntionDescription) {
		this.soluntionDescription = soluntionDescription;
	}

	@Override
	public String toString() {
		return complaintDescription + complaintId;
	}
}
