<%@page import="com.gk.gms.pojoclasses.Complaint"%>
<%@page import="com.gk.gms.java.SeeComplaintDetails"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<script language='javascript'>
function provideSol(comId) {
	
	alert(comId);
    var form = document.forms["solution"];
    form._submit_function_ = form.submit;

    form.setAttribute("method", "get");
    form.setAttribute("action", "ProvideSolution.jsp");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "comId");
        hiddenField.setAttribute("value", comId);

        form.appendChild(hiddenField);

    document.body.appendChild(form);
    form._submit_function_(); //call the renamed function
	
}
</script>
<body BACKGROUND="../images/background.jpg">
	<%
		String manufacturerId = String.valueOf(request.getSession()
				.getAttribute("mId"));

		response.setContentType("text/html");
		List<Complaint> list = SeeComplaintDetails
				.GetComplaintDetails(manufacturerId);
	%>
	<form method="post" name="solution">
		<h2>
			<b><font color=#BBO55E>Please check Complaints raised by
					customer </font> </b>
		</h2>
		<!-- <input type="submit" name="ProvideSolution" value="ProvideSolution"> -->
		<table border="1" cellpadding="0" cellspacing="0" width="60%">
			<th>Provide solution</th>
			<th>Customer Id</th>
			<th>Complaint Type</th>
			<th>Complaint Description</th>

			<%
				for (Complaint info : list) {
			%>
			<tr>
				<td><input type="radio" name="complain"
					onclick="provideSol(<%=info.getComplaintId()%>)"></td>
				<td><%=info.getCustomerId()%></td>
				<td><%=info.getComplaintType()%></td>
				<td><%=info.getComplaintDescription()%></td>
				<%
					}
				%>
			
		</table>
	</form>
</body>
</html>