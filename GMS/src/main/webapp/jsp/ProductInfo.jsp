<%@page import="com.gk.gms.java.ValidateCustomerProduct"%>
<%@page import="com.gk.gms.pojoclasses.ProductInfo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body BACKGROUND="../images/background.jpg">
	<%
		String customerId = request.getParameter("customerId");

		response.setContentType("text/html");
		List<ProductInfo> list = ValidateCustomerProduct
				.verifyCustomerProductRequest(customerId);
	%>
	<table border="1" cellpadding="0" cellspacing="0" width="60%">
		<th>Product Name</th>
		<th>Product Type</th>
		<th>Product Cost</th>

		<%
			for (ProductInfo info : list) {
		%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=info.getName()%></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=info.getType()%></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=info.getCost()%></td>
			<%
				}
			%>
		
	</table>
</body>
</html>