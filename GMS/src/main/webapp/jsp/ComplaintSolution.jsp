<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ComplaintSolution</title>
</head>
<body BACKGROUND="../images/background.jpg">

	<form action="/gadgetmonitoringsystem/ComplaintSolutionServlet" method="post">
		<table border="0" cellpadding="0" cellspacing="0" width="80%">
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">Customer
						Id&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><input type="text" name="customerId" size="15"></td>
			</tr>
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">To
						Customer MailId &nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><input type="text" name="customerMailId"
					size="15"></td>
			</tr>
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">Solution
						Description&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%">
					<p align="left">
						<textarea rows="4" name="solutionDescription" cols="24"></textarea>
					</p>
				</td>
			</tr>
			<TR>
				<td width="50%"></td>
				<td width="50%"><input type="submit" name="submit"
					value="submit" size="15"></td>
			</TR>
		</table>
	</form>
</body>
</html>