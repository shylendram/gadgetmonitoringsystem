<%@page import="com.gk.gms.pojoclasses.Customer"%>
<%@page import="com.gk.gms.pojoclasses.Manufacturer"%>
<%@page import="java.util.List"%>
<%@page import="com.gk.gms.java.EntityManagerProvider"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Complaint</title>
</head>
<body BACKGROUND="../images/background.jpg">

	<%
		List<Customer> customerList = EntityManagerProvider.getEntityManager().createQuery("from Customer").getResultList();
		List<Manufacturer> manufacturerList = EntityManagerProvider.getEntityManager().createQuery("from Manufacturer").getResultList();
	%>

	<form action="/gadgetmonitoringsystem/ComplaintServlet" method="post">
		<table border="0" cellpadding="0" cellspacing="0" width="80%">
		<%-- 	<tr>
				<td width="50%" align="right"><font color="blue" size="4">Customer
						Id&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><select name="customerId" length="20">
						<%
							if (customerList != null && customerList.size() > 0) {
								for (Customer c : customerList) {
						%>
						<option value="<%=c.getCid()%>"><%=c.getCid()%></option>
						<%
							}
							}
						%>
				</select></td>
			</tr> --%>
			<!-- <tr>
				<td width="50%" align="right"><font color="blue" size="4">Complaint
						Id&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><input type="text" name="complaintId" size="15"></td>
			</tr> -->
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">Complaint
						Type&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><input type="text" name="complaintType"
					size="15"></td>
			</tr>
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">Manufacturer
						Id &nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%"><select name="manufacturerId" length="20">
						<%
							if (manufacturerList != null && manufacturerList.size() > 0) {
								for (Manufacturer m : manufacturerList) {
						%>
						<option value="<%=m.getMid()%>"><%=m.getMid()%></option>
						<%
							}
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td width="50%" align="right"><font color="blue" size="4">Complaint
						Description&nbsp;&nbsp;&nbsp;</font></td>
				<td width="50%">
					<p align="left">
						<textarea rows="4" name="complaintDescription" cols="24"></textarea>
					</p>
				</td>
			</tr>
			<TR>

				<td width="50%"></td>
				<td width="50%"><input type="submit" name="submit"
					value="submit" size="15"></td>
			</TR>
		</table>
	</form>
</body>
</html>