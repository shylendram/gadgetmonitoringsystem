<%@page import="com.gk.gms.java.ManufacturerCustomerDetails"%>
<%@page import="com.gk.gms.pojoclasses.Customer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body BACKGROUND="../images/background.jpg">
<%
String manufacturerId = request.getParameter("manufacturerId");

response.setContentType("text/html");
List<Customer> list = ManufacturerCustomerDetails.getManufacturerCustomerDetails(manufacturerId);


%>
<h2><b><font color=#BBO55E>My Customers  </font> </b> </h2>
<table border="1" cellpadding="0" cellspacing="0" width="60%">
<th>Customer firstname</th>
<th>Customer lastname</th>
<th>Customer Mail</th>


<%
for (Customer info : list) {
	
%>
<tr>
<td><%= info.getFirstname() %></td>
<td><%= info.getLastname() %></td>
<td><%= info.getMail() %></td>
<%	
}
%>

</table>
</body>
</html>