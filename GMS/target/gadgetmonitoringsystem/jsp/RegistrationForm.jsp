<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Registration Page</title>
</head>
<body background="../images/background.jpg">
	<h2>
		<font color=#BB055E><center>
				<img src="../images/a.GIF">&nbsp;Registration Form&nbsp;<img
					src="../images/a.GIF">
			</center></font>
	</h2>
	<form action="RegistrationServlet" method="post">
		<hr color=#BB055E>
		<table align="center">
			<br>
			<br>
			<tr>
				<font color=blue><h3>
						<center>
							<i>All Red * Fields Are Required</i>
						</center>
					</h3></font>
			</tr>
			<tr>
				<td><b><font color=#BB055E>First Name:</font></b></td>
				<td><b><input name="firstname" size=15></td>
				<td><b><font color=red>*</font></b></td>
			</tr>
			<tr>
				<td><b><font color=#BB055E>Last Name:</font></b></td>
				<td><b><input name="firstname" size=15></td>
				<td><b><font color=red>*</font></b></td>
			</tr>
			<tr>
				<td><b><font color=#BB055E>User Name:</font></b></td>
				<td><b><input name="username" size=15></td>
				<td><b><font color=red>*</font></b></td>
			</tr>
			<tr>
				<td><b><font color=#BB055E>Password:</font></b></td>
				<td><b><input name="password" type=password size=15></td>
				<td><b><font color=red>*</font></b></td>
			</tr>

			<tr>
				<td><b><font color=#BB055E>Mail Account:</font></b></td>
				<td><b><input name="mail" size=15></td>
				<td><b><font color=red>*</font></b></td>
			</tr>
			<td><b><font color=#BB055E>User Type:</font></b></td>
			<td><b> <select name="usertype">
						<option>...select user type...</option>
						<option name="service_provider">service_provider</option>
						<option name="customer">customer</option>
						<option name="manufacturer">manufacturer</option>
				</select></td>
			<td><b><font color=red>*</font></b></td>
			</tr>

			<tr>
				<td></td>
				<td><br> <br> <input type="submit" name="Register"
					value="Register"></td>
			</tr>
		</table>
</body>
</html>